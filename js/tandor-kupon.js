const coupon = document.querySelector(".coupon");
const getCoupon = document.querySelector(".coupon__button");
const myModal = document.querySelector("#myModal");
const closeModal = document.querySelector(".close");
const submitSuccess = document.querySelector("#submitSuccess");
const formModal = document.querySelector("#formModal");
const successModal = document.querySelector(".send-success");
const inputValue = document.querySelectorAll(".input__control");

const errorText = document.querySelectorAll(".input-validation__message");

getCoupon.addEventListener("click", () => {
  formModal.style.display = "block";
  myModal.style.display = "block";
});
closeModal.addEventListener("click", () => {
  myModal.style.display = "none";
  successModal.style.display = "none";
});

submitSuccess.addEventListener("click", (el) => {
  inputValue.forEach((item, index) => {
    item.addEventListener("input", () => {
      if (item.value !== "" && inputValue[2].checked) {
        errorText[index].style.display = "none";
      } else {
        errorText[index].style.display = "block";
      }
    });
    item.value !== "" && inputValue[2].checked
      ? (errorText[index].style.display = "none")
      : (errorText[index].style.display = "block");
  });

  if (inputValue[0].value !== "" && inputValue[1].value !== "") {
    el.preventDefault();
    formModal.style.display = "none";
    successModal.style.display = "block";
  }
});
